﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioProducto
    {
        private ConexionSQL conec1;

        public ConexionSQL Conec1 { get => conec1; set => conec1 = value; }

        public void configurarConexion()
        {
            this.Conec1 = new ConexionSQL();
            this.Conec1.NombreBaseDatos = "Smart_factory";
            this.Conec1.NombreTabla = "Producto";
            this.Conec1.CadenaConexion = "Data Source=DESKTOP-U7RM1GB;Initial Catalog=Prueba;Integrated Security=True";
        }

        public void insertarProducto(Producto producto)
		{
			this.configurarConexion();
			this.Conec1.EsSelect = false;
			this.Conec1.conectar();
		}


		public void eliminarProducto(String codigo)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "DELETE FROM " + this.Conec1.NombreTabla
                                    + " WHERE codigo = '" + codigo + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }

        public void actualizarProducto(Producto producto)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "UPDATE " + this.Conec1.NombreTabla
                                    + " SET"
                                    + " nombre = '" + producto.Nombre
                                    + "' WHERE codigo = '" + producto.Codigo1 + "';";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();
        }


        public DataSet retornaProducto()
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla;
                                    
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            return this.Conec1.DbDataSet;
        }


        public Producto buscarProducto(String codigo)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla
                                    + " WHERE codigo = '" + codigo + "';";
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            Producto auxProducto = new Producto();
            DataTable dt = new DataTable();
            dt = this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla];

            try
            {
                auxProducto.Codigo1 = (String) dt.Rows[0]["Codigo"];
                auxProducto.Nombre = (String)dt.Rows[0]["Nombre"];
            }
            catch (Exception ex)
            {
                auxProducto.Codigo1 = "";
                auxProducto.Nombre = "";
                
            }
            return auxProducto;
        }

        public Producto retornaPosicioProducto(int pos)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM " + this.Conec1.NombreTabla;
                                    
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();
            Producto auxProducto = new Producto();
            DataTable dt = new DataTable();
            dt = this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla];

            try
            {
                auxProducto.Codigo1 = (String)dt.Rows[pos]["Codigo"];
                auxProducto.Nombre = (String)dt.Rows[pos]["Nombre"];
            }
            catch (Exception ex)
            {
                auxProducto.Codigo1 = "";
                auxProducto.Nombre = "";

            }
            return auxProducto;
        }

    } //Fin Clase
} //namespace
