﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GUIWeb
{
    public partial class WebMantenedorProducto : System.Web.UI.Page
    {
		public object txtCodigo { get; private set; }

		protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btoGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Producto auxProducto = new Producto();
                auxProducto.Codigo1 = this.txtCodigo.Text;
                auxProducto.Nombre = this.txtNombre.Text;
				



                NegocioProducto auxNegocio = new NegocioProducto();

                if (String.IsNullOrEmpty(auxNegocio.buscarProducto(auxProducto.Codigo1).Codigo1))
                {

                    auxNegocio.insertarProducto(auxProducto);
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<script language=Javascript>\n");
                    sb.Append("window.alert('Datos Guardados'); \n");
                    sb.Append("</script>");
                    Page.RegisterStartupScript("script", sb.ToString());
                    this.txtCodigo = String.Empty;
                    this.txtNombre.Text = String.Empty;
                   

                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<script language=JavaScript>\n");
                    sb.Append("window.alert('Cliente ya existe'); \n");
                    sb.Append("</script>");
                    Page.RegisterStartupScript("script", sb.ToString());
                }

            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<script language=JavaScript>\n");
                sb.Append("window.alert('Datos No guardados'); \n");
                sb.Append("</script>");
                Page.RegisterStartupScript("script", sb.ToString());
            }
        }

        protected void btoLista_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<script language=JavaScript>\n");
            sb.Append("location.href('http://localhost:50196/WebFormListadoProducto.aspx'); \n");
            sb.Append("</script>");
            Page.RegisterStartupScript("script", sb.ToString());

        }
    }
}