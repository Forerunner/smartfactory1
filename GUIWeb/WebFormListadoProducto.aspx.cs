﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GUIWeb
{
    public partial class WebFormListadoProducto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btoMostrar_Click(object sender, EventArgs e)
        {
            NegocioProducto auxNegocio = new NegocioProducto();

            this.GridViewCliente.DataSource = auxNegocio.retornaProducto();
            this.GridViewCliente.DataMember = "producto";
            this.GridViewCliente.DataBind();
			
        }
    }
}