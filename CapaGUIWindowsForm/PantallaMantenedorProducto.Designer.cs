﻿namespace CapaGUIWindowsForm
{
    partial class PantallaMantenedorProducto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.btoGrabar = new System.Windows.Forms.Button();
            this.btoEliminar = new System.Windows.Forms.Button();
            this.btoActualizar = new System.Windows.Forms.Button();
            this.btoListar = new System.Windows.Forms.Button();
            this.btoSalir = new System.Windows.Forms.Button();
            this.btoPrimero = new System.Windows.Forms.Button();
            this.btoAnterior = new System.Windows.Forms.Button();
            this.txtPosicion = new System.Windows.Forms.TextBox();
            this.btoSiguiente = new System.Windows.Forms.Button();
            this.btoUltimo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(68, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Codigo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(71, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(219, 66);
            this.txtCodigo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(192, 26);
            this.txtCodigo.TabIndex = 2;
            this.txtCodigo.TextChanged += new System.EventHandler(this.TxtCodigo_TextChanged);
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(219, 146);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(192, 26);
            this.txtNombre.TabIndex = 3;
            // 
            // btoGrabar
            // 
            this.btoGrabar.Location = new System.Drawing.Point(74, 264);
            this.btoGrabar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btoGrabar.Name = "btoGrabar";
            this.btoGrabar.Size = new System.Drawing.Size(84, 29);
            this.btoGrabar.TabIndex = 4;
            this.btoGrabar.Text = "Grabar";
            this.btoGrabar.UseVisualStyleBackColor = true;
            this.btoGrabar.Click += new System.EventHandler(this.btoGrabar_Click);
            // 
            // btoEliminar
            // 
            this.btoEliminar.Location = new System.Drawing.Point(189, 262);
            this.btoEliminar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btoEliminar.Name = "btoEliminar";
            this.btoEliminar.Size = new System.Drawing.Size(84, 29);
            this.btoEliminar.TabIndex = 5;
            this.btoEliminar.Text = "Eliminar";
            this.btoEliminar.UseVisualStyleBackColor = true;
            // 
            // btoActualizar
            // 
            this.btoActualizar.Location = new System.Drawing.Point(304, 264);
            this.btoActualizar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btoActualizar.Name = "btoActualizar";
            this.btoActualizar.Size = new System.Drawing.Size(90, 29);
            this.btoActualizar.TabIndex = 6;
            this.btoActualizar.Text = "Actualizar";
            this.btoActualizar.UseVisualStyleBackColor = true;
            // 
            // btoListar
            // 
            this.btoListar.Location = new System.Drawing.Point(418, 262);
            this.btoListar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btoListar.Name = "btoListar";
            this.btoListar.Size = new System.Drawing.Size(84, 29);
            this.btoListar.TabIndex = 7;
            this.btoListar.Text = "Listar";
            this.btoListar.UseVisualStyleBackColor = true;
            this.btoListar.Click += new System.EventHandler(this.btoListar_Click);
            // 
            // btoSalir
            // 
            this.btoSalir.Location = new System.Drawing.Point(533, 262);
            this.btoSalir.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btoSalir.Name = "btoSalir";
            this.btoSalir.Size = new System.Drawing.Size(84, 29);
            this.btoSalir.TabIndex = 8;
            this.btoSalir.Text = "Salir";
            this.btoSalir.UseVisualStyleBackColor = true;
            this.btoSalir.Click += new System.EventHandler(this.btoSalir_Click);
            // 
            // btoPrimero
            // 
            this.btoPrimero.Location = new System.Drawing.Point(71, 321);
            this.btoPrimero.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btoPrimero.Name = "btoPrimero";
            this.btoPrimero.Size = new System.Drawing.Size(84, 29);
            this.btoPrimero.TabIndex = 9;
            this.btoPrimero.Text = "<|";
            this.btoPrimero.UseVisualStyleBackColor = true;
            this.btoPrimero.Click += new System.EventHandler(this.btoPrimero_Click);
            // 
            // btoAnterior
            // 
            this.btoAnterior.Location = new System.Drawing.Point(189, 321);
            this.btoAnterior.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btoAnterior.Name = "btoAnterior";
            this.btoAnterior.Size = new System.Drawing.Size(84, 29);
            this.btoAnterior.TabIndex = 10;
            this.btoAnterior.Text = "<";
            this.btoAnterior.UseVisualStyleBackColor = true;
            this.btoAnterior.Click += new System.EventHandler(this.btoAnterior_Click);
            // 
            // txtPosicion
            // 
            this.txtPosicion.Enabled = false;
            this.txtPosicion.Location = new System.Drawing.Point(304, 321);
            this.txtPosicion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPosicion.Name = "txtPosicion";
            this.txtPosicion.Size = new System.Drawing.Size(84, 26);
            this.txtPosicion.TabIndex = 11;
            this.txtPosicion.TextChanged += new System.EventHandler(this.TxtPosicion_TextChanged);
            // 
            // btoSiguiente
            // 
            this.btoSiguiente.Location = new System.Drawing.Point(418, 321);
            this.btoSiguiente.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btoSiguiente.Name = "btoSiguiente";
            this.btoSiguiente.Size = new System.Drawing.Size(84, 29);
            this.btoSiguiente.TabIndex = 12;
            this.btoSiguiente.Text = ">";
            this.btoSiguiente.UseVisualStyleBackColor = true;
            this.btoSiguiente.Click += new System.EventHandler(this.btoSiguiente_Click);
            // 
            // btoUltimo
            // 
            this.btoUltimo.Location = new System.Drawing.Point(533, 321);
            this.btoUltimo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btoUltimo.Name = "btoUltimo";
            this.btoUltimo.Size = new System.Drawing.Size(84, 29);
            this.btoUltimo.TabIndex = 13;
            this.btoUltimo.Text = "|>";
            this.btoUltimo.UseVisualStyleBackColor = true;
            this.btoUltimo.Click += new System.EventHandler(this.btoUltimo_Click);
            // 
            // PantallaMantenedorProducto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 562);
            this.Controls.Add(this.btoUltimo);
            this.Controls.Add(this.btoSiguiente);
            this.Controls.Add(this.txtPosicion);
            this.Controls.Add(this.btoAnterior);
            this.Controls.Add(this.btoPrimero);
            this.Controls.Add(this.btoSalir);
            this.Controls.Add(this.btoListar);
            this.Controls.Add(this.btoActualizar);
            this.Controls.Add(this.btoEliminar);
            this.Controls.Add(this.btoGrabar);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PantallaMantenedorProducto";
            this.Text = "PantallaMantenedorProducto";
            this.Load += new System.EventHandler(this.PantallaMantenedorCliente_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button btoGrabar;
        private System.Windows.Forms.Button btoEliminar;
        private System.Windows.Forms.Button btoActualizar;
        private System.Windows.Forms.Button btoListar;
        private System.Windows.Forms.Button btoSalir;
        private System.Windows.Forms.Button btoPrimero;
        private System.Windows.Forms.Button btoAnterior;
        private System.Windows.Forms.TextBox txtPosicion;
        private System.Windows.Forms.Button btoSiguiente;
        private System.Windows.Forms.Button btoUltimo;
    }
}