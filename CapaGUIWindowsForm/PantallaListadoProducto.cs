﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaGUIWindowsForm
{
    public partial class PantallaListadoCliente : Form
    {
        public PantallaListadoCliente()
        {
            InitializeComponent();
        }

        private void btoSalir_Click(object sender, EventArgs e)
        {
            this.Dispose();
            System.GC.Collect();
        }

        private void btoMostrar_Click(object sender, EventArgs e)
        {
            NegocioProducto auxNegocio = new NegocioProducto();
            this.dataGridViewCliente.DataSource = auxNegocio.retornaProducto();
            this.dataGridViewCliente.DataMember = "Cliente";
        }
    }
}
