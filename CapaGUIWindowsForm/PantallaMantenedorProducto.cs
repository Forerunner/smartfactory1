﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaGUIWindowsForm
{
    public partial class PantallaMantenedorProducto : Form
    {
        private int posicion;
        private int ultimo;

        public PantallaMantenedorProducto()
        {
            InitializeComponent();
        }

        public int Posicion { get => posicion; set => posicion = value; }
        public int Ultimo { get => ultimo; set => ultimo = value; }


        public void limpiar()
        {
            this.txtCodigo.Clear();
            this.txtNombre.Clear();
            this.txtCodigo.Focus();
        }

        public void mostrar()
        {
            NegocioProducto auxNegocio = new NegocioProducto();
            Producto auxProducto = new Producto();
            this.Ultimo = auxNegocio.retornaProducto().Tables["Producto"].Rows.Count - 1;

            if (this.Posicion <= 0)
                this.Posicion = 0;
            if (this.Posicion >= this.Ultimo)
                this.Posicion = this.Ultimo;

            auxProducto = auxNegocio.retornaPosicioProducto(this.Posicion);

            this.txtCodigo.Text = auxProducto.Codigo1;
            this.txtNombre.Text = auxProducto.Nombre;

            this.txtPosicion.Text = (this.Posicion+1) + "-" + (this.Ultimo+1);

        }


        private void btoGrabar_Click(object sender, EventArgs e)
        {
            if (this.btoGrabar.Text.Equals("Nuevo"))
            {
                this.habilitar();
                this.limpiar();
                this.btoGrabar.Text = "Grabar";
                this.btoEliminar.Enabled = false;
                this.btoActualizar.Enabled = false;
                this.btoListar.Enabled = false;
                this.btoSalir.Text = "Cancelar";
                this.btoPrimero.Enabled = false;
                this.btoAnterior.Enabled = false;
                this.btoSiguiente.Enabled = false;
                this.btoUltimo.Enabled = false;
            }
            else
            {
                try
                {
                    Producto auxProducto = new Producto();
					auxProducto.Codigo1 = this.txtCodigo.Text;
					auxProducto.Nombre = this.txtNombre.Text;

                    
                    NegocioProducto auxNegocio = new NegocioProducto();

                    if (String.IsNullOrEmpty(auxNegocio.buscarProducto(auxProducto.Codigo1).Codigo1))
                    {

                        auxNegocio.insertarProducto(auxProducto);
                        MessageBox.Show("Datos Guardados ", "Sistema");
                    }
                    else
                    {
                        MessageBox.Show("Producto ya existe ", "Sistema");
                    }

                       this.desHabilitar();
                    this.mostrar();
                    this.btoGrabar.Text = "Nuevo";
                    this.btoEliminar.Enabled = true;
                    this.btoActualizar.Enabled = true;
                    this.btoListar.Enabled = true;
                    this.btoSalir.Text = "Salir";
                    this.btoPrimero.Enabled = true;
                    this.btoAnterior.Enabled = true;
                    this.btoSiguiente.Enabled = true;
                    this.btoUltimo.Enabled = true;



                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al guardar " + ex.Message, "Sistema");
                }
            }
        }

        public void habilitar()
        {
            this.txtCodigo.Enabled = true;
            this.txtNombre.Enabled = true;
        }

        public void desHabilitar()
        {
            this.txtCodigo.Enabled = false;
            this.txtNombre.Enabled = false;
        }



        private void PantallaMantenedorCliente_Load(object sender, EventArgs e)
        {
            this.desHabilitar();
            this.Posicion = 0;
            this.mostrar();
            this.btoGrabar.Text = "Nuevo";
        }

        private void btoSiguiente_Click(object sender, EventArgs e)
        {
            this.Posicion = this.Posicion + 1;
            this.mostrar();
        }

        private void btoAnterior_Click(object sender, EventArgs e)
        {
            this.Posicion = this.Posicion - 1;
            this.mostrar();

        }

        private void btoPrimero_Click(object sender, EventArgs e)
        {
            this.Posicion = 0;
            this.mostrar();

        }

        private void btoUltimo_Click(object sender, EventArgs e)
        {
            this.Posicion = this.Ultimo;
            this.mostrar();
        }

        private void btoListar_Click(object sender, EventArgs e)
        {
            PantallaListadoCliente pListado = new PantallaListadoCliente();
            pListado.ShowDialog();
        }

        private void btoSalir_Click(object sender, EventArgs e)
        {
            if (this.btoSalir.Text.Equals("Salir"))
            {
                this.Dispose();
                System.GC.Collect();
            }
            else
            {
                this.desHabilitar();
                this.mostrar();
                this.btoGrabar.Text = "Nuevo";
                this.btoEliminar.Enabled = true;
                this.btoActualizar.Enabled = true;
                this.btoListar.Enabled = true;
                this.btoSalir.Text = "Salir";
                this.btoPrimero.Enabled = true;
                this.btoAnterior.Enabled = true;
                this.btoSiguiente.Enabled = true;
                this.btoUltimo.Enabled = true;


            }
        }

        private void TxtPosicion_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtCodigo_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
