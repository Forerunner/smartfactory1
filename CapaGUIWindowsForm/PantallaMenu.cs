﻿using SHDocVw;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaGUIWindowsForm
{
    public partial class PantallaMenu : Form
    {
        public PantallaMenu()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PantallaMantenedorProducto pMantenedor = new PantallaMantenedorProducto();
            pMantenedor.ShowDialog();
        }

        private void clienteWebToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InternetExplorer auxExplorer = new InternetExplorer();

            Object auxObjeto = new Object();
            auxExplorer.Navigate("http://localhost:50196/WebFormMenu.aspx", ref auxObjeto, ref auxObjeto, ref auxObjeto, ref auxObjeto);
            auxExplorer.Visible = true;

        }
    }
}
