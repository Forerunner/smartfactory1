﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CapaServicio
{
    /// <summary>
    /// Descripción breve de WebServiceMantenedorCliente
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServiceMantenedorProducto : System.Web.Services.WebService
    {

        [WebMethod]
        public void insertarProductoService(Producto producto)
        {
           NegocioProducto auxNegocio = new NegocioProducto();
            auxNegocio.insertarProducto(producto);
        }


        [WebMethod]
        public void eliminarProductoService(String codigo)
        {
            NegocioProducto auxNegocio = new NegocioProducto();
            auxNegocio.eliminarProducto(codigo);
        }

        [WebMethod]
        public void actualizarProductoService(Producto producto)
        {
            NegocioProducto auxProducto = new NegocioProducto();
            auxProducto.actualizarProducto(producto);
        }


        [WebMethod]
        public DataSet retornaProductoService()
        {
            NegocioProducto auxNegocio = new NegocioProducto();
            return auxNegocio.retornaProducto();
        }


        [WebMethod]
        public Producto buscarProductoService(string codigo)
        {
            NegocioProducto auxNegocio = new NegocioProducto();
            return auxNegocio.buscarProducto(codigo);
        }

        [WebMethod]
        public Producto retornaPosicioProductoService(int pos)
        {
            NegocioProducto auxNegocio = new NegocioProducto();
            return auxNegocio.retornaPosicioProducto(pos);
        }


    }
}
